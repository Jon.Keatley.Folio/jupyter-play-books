# Jupyter Play Books

A basic repo for setting up publishable Jupyter books.

## Getting started

After cloning this repo from the root of the repo use `poetry install` to install the required dependencies.

## Commands

Create a book
```bash
jupyter-book create mynewbook/
```

Build a book
```bash
jupyter-book build mybook/
```

Work on a book
```bash
poetry shell
jupyter notebook
```

or

```bash
poetry run jupyter notebook
```

## Things to look at

- https://jupyterlite.readthedocs.io/en/latest/
